<header class="row">
    <label for="doc-drawer-checkbox" class="button drawer-toggle col-md hidden-lg hidden-md" style="display: block; float: right;"></label>
    <span href="https://bbssh.club" class="logo bbssh doc" >[bbssh.club]~$</span>
    <?php
    $links = array(
        array("./index.php", "Home"),
        array("./features.php","Features"),
        array("./rules.php","Rules"),
        array("./users.php" , "User Directory"),
        array("./help.php" , "Help")
    );
    for ($i=0; $i < count($links); $i++) { 
        
        echo '<a href="' . $links[$i][0] . '" class="button col-sm col-md hidden-sm' . ($ACTIVE_TAB == $i ? " header-button-active" : "") . '">' . $links[$i][1] . '</a>';
    }
    ?>
</header>
<div class="hidden-lg hidden-md" >
    <div class="row" id="doc-wrapper">
        <input id="doc-drawer-checkbox" class="drawer" value="off" type="checkbox">
        <nav class="col-md-4 col-lg-3" id="nav-drawer">
            <h3>Menu</h3>
            <label for="doc-drawer-checkbox" class="button drawer-close">
            </label>
            <a href="./index.php" class="">Home</a>
            <a href="./features.php" class="">Features</a>
            <a href="./rules.php" class="">Rules</a>
            <a href="./users.php" class="">User Directory</a>
            <a href="./help.php" class="">Help</a>
        </nav>
    </div>
</div>
