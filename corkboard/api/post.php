<?php
	header('Content-Type: application/json');
	$dati_prova = array(
        "posts"=>array(
            array(
                "date"=> "2020-09-01",
                "title"=> "New homepage!",
                "content"=> "Check out out new responsive homepage at https://bbssh.club",
                "author_name"=> "Celeste",
                "author_mail"=> "celeste@bbssh.club",
                "category_id"=> 1
            ),
            array(
                "date"=> "2020-09-04",
                "title"=> "New games!",
                "content"=> "We added new CLI games",
                "author_name"=> "Celeste",
                "author_mail"=> "celeste@bbssh.club",
                "category_id"=> 2
            )
            )
        );
	echo json_encode($dati_prova);
?>
