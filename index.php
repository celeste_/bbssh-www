<!DOCTYPE html>
<html>

<head>
	<?php include "./common/minicss_head.php" ?>
</head>

<body>
	<?php
	$ACTIVE_TAB = 0;
	include "./common/header.php"
	?>

	<div class="container">

		<div class="row">
			<div class="info col-md-6">
				<h2>What is this place?</h2>
				<p> bbssh.club is a small <a href="https://en.wikipedia.org/wiki/Shell_account">shell server</a>. It's
					purpose is to provide access to a <a href="https://www.freebsd.org/">FreeBSD</a> system, host small
					web pages for each user, and function as a modern UNIX-y <a href="https://en.wikipedia.org/wiki/Bulletin_board_system">BBS-like</a> system of sorts. </p>
			</div>

			<div class="col-md-6">
				<h2> What are the requirements to join?</h2>
				<p> The only requirements are to maintain active membership. You must <a href="./rules.php">follow the
						rules and respect the CoC</a>. In order to improve security, you must use <a href="https://gitlab.com/beegrrl/bbssh/-/blob/master/ssh-keygen-howto.md">ssh key based
						authentication</a> </p>
			</div>
		</div>


		<div class="row">
			<div class="registration col-md-12 fluid card error">
				<h4 class="section"> Registration is currently closed </h4>
				<p> We're currently still in the development phase. Although opening to the general public is the end
					goal, feature completeness and stability are important. </p>
			</div>
		</div>


		<div class="row signup cols-md-12">
			<form action="registration.php" method="POST" onsubmit="return validate();">
				<fieldset disabled="false">
					<h3> Rules Quiz </h3>
					<h3><small> instead of a using captcha, we're verifying humans based on their comprehension of the
							server's rules</small></h3>
					<div id="appendme"></div>
					<label for="name"> Username: </label> </br>
					<input type="text" name="name" id="name" placeholder="what should we call you?" maxlength="16" required><br>
					<label for="email"> E-Mail: </label> </br>
					<input type="text" name="email" id="email" placeholder="how will we contact you?" required><br>
					<label for="sshpubkey"> ssh-pubkey: </label> </br>
					<textarea name="sshpubkey" id="sshpubkey" required rows="5" cols="80" wrap="soft" placeholder="paste your ssh public key here"></textarea> <br>
					<input type="submit" value="Submit">
				</fieldset>
			</form>
		</div>

	</div>

	<?php include "./common/footer.php" ?>
	<script type="text/javascript" src="./assets/js/validate-form.js"></script>
</body>

</html>
