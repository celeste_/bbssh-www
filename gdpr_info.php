<!DOCTYPE html>
<html>

<head>
    <?php include "./common/minicss_head.php" ?>
</head>

<body>
    <?php include "./common/header.php" ?>

    <div class="container">
        <div class="row cols-md-12">
            <div class="card fluid">
                <h3>Privacy Principles<small>An informative cheat sheet</small></h3>
                <ul>
                    <li> On this site all users have to follow GDPR principles. Why? </li>
                    <ul>
                        <li> GDPR is currently one of the most strict data protection laws </li>
                        <li> We have some EU users, so we all need to comply anyway</li>
                        <li> Promoting good privacy principles for non-EU users too promotes freedom around the world
                        </li>
                    </ul>

                    <li> GDPR in short </li>
                    <ul>
                        <li> "Personal data" is ANY information that could identify an individual.</li>
                        <li> Consider also if, during data processing, you could identify them anyway by merging it with
                            other data </li>
                        <li> In many cases you have to ask the user for the permission to collect and use their personal
                            data </li>
                        <li> You need to inform the user about the PURPOSE you'll use data </li>
                        <li> You collect data for a specific purpose? Then you can use them ONLY for that purpose. </li>
                        <li> People have the right to ask a copy of their personal data, modify, correct, delete them
                        </li>
                        <li> Pay <a href="https://gdpr-info.eu/art-9-gdpr/">A LOT</a> of attention when processing data
                            about racial/ethnic origin, sexual orientation, religious or political belief, biometric
                            data and health data. <br>Processing this type of data is normally FORBIDDEN</li>
                        <li> Use <a href="https://gdpr-info.eu/art-25-gdpr/">Data minimisation, Privacy by default,
                                Privacy by design</a> principles. </li>
                    </ul>

                    <li>Other resources</li>
                    <ul>
                        <li> <a href="https://ec.europa.eu/info/sites/info/files/virtual_idenitity_en.pdf">PDF
                                cheatsheet</a> from European Commission's website. See page 3 in particular</li>
                        <li> <a href="https://ec.europa.eu/info/sites/info/files/100124_gdpr_factsheet_mythbusting.pdf">Mythbusting
                                PDF</a> </li>
                        <li> <a href="https://ec.europa.eu/info/law/law-topic/data-protection/reform/rights-citizens_en">Another
                                page</a> about rights for citizens </li>
                    </ul>
                </ul>
            </div>

            <div class="card fluid">
                <h3> How do we handle data</h3>
                <ul>
                    <li>
                        We only collect data needed to operate the service
                        <ul>
                            <li>Your login details, such as username and ssh public key</li>
                            <li>Files you upload, modify or create on the server</li>
                            <li>Data you create or expose by your activity on the server, such as commands you enter</li>
                            <li>Data you add in public systems, such as our public corkboard or a page you host</li>
                            <li>Your IP address. Pay attention, it could be visible to other users too because of UNIX
                                structure</li>
                        </ul>
                    </li>

                    <li>
                        How to modify or delete existing data
                        <ul>
                            <li> You can obtain a copy of your data through ssh/scp, it's all in your directory. If you need
                                help, contact the admin</li>
                            <li> If you want to delete your account and all its data, contact the admin through the
                                internal mail system</li>
                        </ul>
                    </li>

                    <li>Where we store them</li>
                    <ul>
                        <li>The server is hosted at DigitalOcean, with servers located in US</li>
                    </ul>

                    <li>How long we'll store them</li>
                    <ul>
                        <li>Data are stored as long as you're a user of this service</li>
                        <li>The hosting provider could have some backup, so it could need some time for data do be completely deleted</li>
                    </ul>

                    <li>How are data protected</li>
                    <ul>
                        <li>Consider that bbssh.club doesn't ask you for high risk categories of data and it's a hobby
                            project. Don't save important information in it</li>
                        <li>Anyway, access is granted with strong public key authentication only</li>
                        <li>Communication on webpages between server and the user is secured using HTTPS</li>
                        <li>Public homedir, a system to display user's pages, is disabled by default as it can expose
                            your username. This respects the "Privacy by default" principle. You can ask us to activate
                            it</li>
                    </ul>

                </ul>
            </div>

            <div class="card fluid">
                <h3>How do you need to (not) handle data</h3>
                <ul>
                    <li> You can NOT ask for or process sensitive data as defined in Article 9 of GDPR through our
                        server</li>
                    <li> Your personal webpage on this site has to follow GDPR and this site's rules. If you have any doubt, choose
                        the stricter one or ask the admin</li>
                </ul>
            </div>
            <div class="card fluid">
                <h3>Continue reading and sign up</h3>
                <ul>
                    <a class="button col-sm-3" href="/rules.php">Back to the server rules</a>
                </ul>

            </div>

        </div>
    </div>

    <?php include "./common/footer.php" ?>
</body>

</html>