# TODO 
	- fix broken js (which involves)
		+ preventing the form from running action="registration.php" if the validation doesn't check out (probably works, please stress test) 
	- fix broken php
		+ ie prevent users from blowing up the db
		+ take the dummy passwords out of registration.php
	- verify ssh pubkey?
		+ possibly by a "key length range" by the number of characters (which can be problematic since there are many lengths of key)
		+ or, by verifying that the key is within sane length (ie longer than 2048)
		+ might be problematic to call system commands through php, but we can verify that a key is indeed a key using "ssh-keygen -lf /path/to/key/id_rsa.pub" 
	- ???
	- profit

