<!DOCTYPE html>
<html>

<head>
	<?php include "./common/minicss_head.php" ?>
</head>

<body>
<?php
	$ACTIVE_TAB = 2;
	include "./common/header.php"
	?>

	<div class="container">
		<div class="row cols-md-12">

			<div class="card fluid">
				<h3> Rules <small>breaking these will result in a ban</small> </h3>
				<ul>
					<li> You must follow US law </li>
					<ul>
						<li> You are not allowed to use or store malware, viruses, etc on the server</li>
						<li> You are not allowed to access or store illegal material on this server </li>
						<li> You are not allowed to run nmap or other portscan attacks against any server or system you
							don't own</li>
						<li> You are not allowed to run any form of DOS/DDOS attack </li>
					</ul>

					<li> Remember, there are other users on this server. Do not needlessly waste resources by doing
						things like: </li>
					<ul>
						<li> Seeding and downloading torrent files, legal or illegal </li>
						<li> Using this server as a proxy </li>
						<li> Running an irc bouncer or bots </li>
						<li> Running crypto miners </li>
					</ul>

					<li>Be courteous and kind</li>
					<ul>
						<li> Follow all rules of any services and websites you connect you. One person's bad behavior
							can get us all banned </li>
						<li> Do not run any daemons or services </li>
						<li> Do not share accounts </li>
						<li> Do not advertise to other users or via our web hosting </li>
						<li> Do not deliberately break the server. Accidents are forgivable, forkbombs are not </li>
						<li> Do not dox other users, respect their privacy </li>
					</ul>
				</ul>
			</div>

			<div class="card fluid">
				<h3>Privacy Principles<small>protecting people</small></h3>
				<ul>
					<li> On this site all users have to follow GDPR principles. Why? </li>
					<ul>
						<li> GDPR is currently one of the most strict data protection laws </li>
						<li> We have some EU users, so we all need to comply anyway</li>
						<li> Promoting good privacy principles for non-EU users too promotes freedom around the world</li>
					</ul>
					<li>Find out how we handle data, your rights and your duties</li>
					<a class="button" href="/gdpr_info.php">More on GDPR</a>
				</ul>
			</div>
		</div>

		<div class="card fluid">
			<h3> Code of Conduct <small>breaking these guidelines will result in a general warning, then a ban </small>
			</h3>
			<p> In order to ensure a welcoming and respectful community, we ask you to be kind, patient, friendly, and
				considerate. When talking to others carefully choose your words. When disagreements happen, remember to
				be
				empathetic. We want to assure inclusiveness to all people. This includes all people of any race,
				ethnicity,
				cultural background, nationality, color of skin, socieoeconomic class, education level, gender, mode of
				attraction, age, body type, physical and mental ability, and presence or absence of religious belief.
				This
				isn't a safe space for "muh free speech". Discriminatory talk isn't acceptable in any capacity. </p>
		</div>

	</div>


	<?php include "./common/footer.php" ?>
</body>

</html>