<!DOCTYPE html>
<html>

<head>
	<?php include "./common/minicss_head.php" ?>
</head>

<body>
	<?php
	$ACTIVE_TAB = 4;
	include "./common/header.php"
	?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="collapse">
					<!--  <input type="radio" id="accordion-section3" aria-hidden="true" name="accordion">
			<label for="accordion-section3" aria-hidden="true"></label>
			<div> </div> -->

					<input type="radio" id="accordion-section1" checked aria-hidden="true" name="accordion">
					<label for="accordion-section1" aria-hidden="true">How do I use ssh?</label>
					<div>
						<h2> How do I use ssh? </h2>
						<h4> setting up a key</h4>
						<p> If you're on Linux or UNIX a Mac, open your terminal and run the following commands. ssh is probably already installed. </p>
						<pre>$ ssh-keygen </pre>
						<p> after running this command, you'll be walked through a setup. Respond to the prompts or just mash the enter key to accept the defaults. This process looks like the following: </p>
						<pre>you@localhost:~ $ ssh-keygen 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/you/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/you/.ssh/id_rsa.
Your public key has been saved in /home/you/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx you@localhost
The key's randomart image is:
+---[RSA 2048]----+
|      _____      |
|    .'     '.    |
|   /  o   o  \   |
|  |           |  |
|  |  \     /  |  |
|   \  '---'  /   |
|    '._____.'    |
|                 |
|                 |
+----[SHA256]-----+ </pre>

						<h4> The box says "ssh pubkey". Where is my ssh pubkey?</h4>
						<p>To view your ssh public key, you can run the following command. You can share this one with computers you trust, but please <b>never share your private key</b> </p>
						<pre>$ cat ~/.ssh/id_rsa.pub</pre>

						<h4> How do I connect using SSH? </h4>
						<pre> $ ssh yourusername@bbssh.club</pre>
						<p>you are now at a command prompt on the remote machine</p>
						<figure>
							<img src="./assets/images/ssh_astral.jpg" style="width: 50%;" alt="remote login is a lot like astral projection " />
							<figcaption> ssh is astral projection for nerds</figcaption>
						</figure>
					</div>

					<input type="radio" id="accordion-section2" aria-hidden="true" name="accordion">
					<label for="accordion-section2" aria-hidden="true"> How do I use mail(1)? </label>
					<div>
						<h4> mail(1) -> or "wait, this is actually ed???" </h4>
						<p>After some time struggling to understand mail(1), I've decided to write a "all you need to know in 15 minutes or less" guide. If you've never used a line editor (like ed or vim's line mode) it might be a bit confusing. Mail is sparan. Mail is not mutt or alpine. But you're in luck -- this crash course guide will help.</p>

						<h4> How do I read mail?</h4>
						<p>upon login, you might see "you have mail". In order to read it run</p>
						<pre>$ mail</pre>
						<p>now you ask yourself "what the hell is going on?" Each message is indexed by a number. Typing the message number and pressing return will not only **select the message** but also **print the message**. Other ways to print the message include: </p>
						<pre>& p $message_number </br>& t $message_number </pre>

						<h4> I want to open mail(1) but it keeps saying "no mail for $user" </h4>
						<pre>$ mail -f </pre>

						<h4> How do I reply to mail?</h4>
						<p>Select the message you want to reply to by typing it's number, then return. To 'reply all' type 'r', to reply to only the sender type 'R', then press return. You are now in a text buffer of sorts. Type out whatever you want. When you are done press enter then Control+d to send the message. If you want to abort you can press Control+c twice</p>


						<h4> How do I send mail?</h4>
						<p>Sending mail is easy! To send mail you will type 'm' followed by a list of users you want to send it to then press enter. Example:</p>
						<pre>& m foo@localhost bar@localhost baz@localhost</pre>
						<p>Now you'll see a subject line. Type the subject then press enter. You're now in the text buffer. Type all the things you want to say. When you're ready to send press enter then Control+d to send. If you want to abort you can press Control+c twice</p>

						<h4> How do I delete mail?</h4>

						<pre>& d $message_number</pre>

						<h4> How do I undelete mail?</h4>

						<pre>& u $message_number </pre>

						<h4>How do I save mail?</h4>

						<pre>& s $message_number saved-message.txt </pre>

						<h4> How do I exit? Control+c isn't working? </h4>
						<p>type Control+d or q</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include "./common/footer.php" ?>
</body>

</html>