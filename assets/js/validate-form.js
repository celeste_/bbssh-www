//quiz shit 
function shuffle (array) {
    var i = 0
    , j = 0
    , temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
	j = Math.floor(Math.random() * (i + 1))
	temp = array[i]
	array[i] = array[j]
	array[j] = temp
    }
}


var questions = [
    [1, "I must follow US law.", "T"],
    [2, "I will not store malware and viruses on the server.", "T"],
    [3, "I am allowed to share, store, or access illegal material via this server?", "F"],
    [4, "It is okay to run portscan attacks against any server and systems I don't own.", "F"],
    [5, "DOS and DDOS attacks are stricly prohibited.", "T"],
    [6, "It is okay to seed and download torrent files using bbssh.club.", "F"],
    [7, "I am allowed to use this server as a proxy.", "F"],
    [8, "I am allowed to run an irc bouncer or bots.", "F"],
    [9, "I am not allowed to run crypto miners on bbssh.club.", "T"],
    [10, "I am allowed to break rules for other services if I connect to them via bbssh.club.", "F"],
    [11, "I am not allowed to run unapproved daemons and services.", "T"],
    [12, "I should not share my account.", "T"],
    [13, "I am allowed to advertise via bbssh.club.", "F"],
    [14, "I will not deliberately break the server.", "T"],
    [15, "I can mean to other users.", "F"],
];

shuffle(questions);

var i;
var j;
for (i=0; i<3; i++){

    // create question prompt
    console.log(questions[i]);
    var para = document.createElement("p");
    para.setAttribute("id", "question"+i);
    para.innerHTML=questions[i][1];
    document.getElementById("appendme").appendChild(para);
    var br = document.createElement("br");
    document.getElementById("question"+i).appendChild(br)

    //create T radio box
    var radio1=document.createElement("input");
    radio1.setAttribute("type", "radio");
    radio1.setAttribute("name", questions[i][0]);
    radio1.setAttribute("id", "radioT"+i);
    radio1.setAttribute("value", "T");
    // create T label
    label1 = document.createElement("label");
    label1.setAttribute("id", "labelT"+i);
    label1.innerHTML="True";
    // add a linebreak
    var br1 = document.createElement("br");
    br1.setAttribute("id", "brT"+i);

    //create F radio box
    var radio2=document.createElement("input");
    radio2.setAttribute("type", "radio");
    radio2.setAttribute("name", questions[i][0]);
    radio2.setAttribute("id", "radioF"+i);
    radio2.setAttribute("value", "F");
    // create F label
    label2 = document.createElement("label");
    label2.setAttribute("id", "labelF"+i);
    label2.innerHTML="False";
    // add a linebreak
    var br2 = document.createElement("br");
    br2.setAttribute("id", "brF"+i);

    document.getElementById("question"+i).appendChild(label1);
    document.getElementById("labelT"+i).appendChild(radio1);
    document.getElementById("labelT"+i).appendChild(br1);

    document.getElementById("question"+i).appendChild(label2);
    document.getElementById("labelF"+i).appendChild(radio2);
    document.getElementById("labelF"+i).appendChild(br2);

    // document.getElementById("question"+i).appendChild(radio1)
    // document.getElementById("radioT"+i).appendChild(label1)
    // document.getElementById("question"+i).appendChild(br1)

    // document.getElementById("question"+i).appendChild(radio2)
    // document.getElementById("radioF"+i).appendChild(label2)
    // document.getElementById("question"+i).appendChild(br2)
};






// verify user input shit
function validateUsername(inputtxt) { 
    var letters = /^[0-9a-zA-Z]+$/;
    if (letters.test(inputtxt) && inputtxt.length <=16 ) {
	return true;
    } else {
	return false;
    }
};

function nameLength(inputtxt) {
    var length = inputtxt.length;
    if (inputtxt.length <= 16) {
	return true;
    } else {
	return false;
    }
};

function validateEmail(inputtxt) {
    //return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(inputtxt).toLowerCase());
};

function validate(){
    var name = document.getElementById('name').value;
    var namevalidation = validateUsername(name);
    var email = document.getElementById('email').value;
    var emailvalidation = validateEmail(email);
    //var sshpubkey = document.getElementById('sshpubkey').value;


    function checkQuiz(){
	var i;
	var j;
	for (i=0; i<3; i++){
	    j=i+1;
	    var answer = questions[i][2];
	    var tradio = document.getElementById("radioT"+i);
	    var fradio = document.getElementById("radioF"+i);

	    //var rbs = document.getElementsByName(questions[i][0]);
	    //console.log(rbs.value);
	    //for (const rb of rbs) {
	    //	if (rb.checked != undefined) {
	    //	    selectedValue = rb.value;
	    //	    console.log(rb.value);
	    //	    break;
	    //	} else {
	    //	    return false;
	    //	}
	    //}

	    var selectedValue;

	    if (tradio.checked === true){
		selectedValue = tradio.value;
	    } else if (fradio.checked === true){
		selectedValue = fradio.value;
	    } else {
		return false;
	    }
		

	 

	    if(selectedValue == null){
		console.log("question " + i + " is incorrect");
		return false;
	    }else if(selectedValue === questions[i][2]){
		console.log("question " + i + " is correct");
	    }else{
		console.log("question " + i + " is incorrect");
		return false;
	    }
	}
    };

    var quizresult = checkQuiz();
    if(quizresult === false){
	alert('Oh No! You failed the quiz. Please re-read the rules and try again')
	return false;
    } else if(namevalidation === false){ 
	alert('Your username can only contain alphanumeric characters. No special characters or spaces are alowed.');
	return false;
    } else if (emailvalidation === false){
	alert("Your email address is invalid");
	return false;
    }else
	return true;
};
