<!DOCTYPE html>
<html>

<head>
	<?php include "./common/minicss_head.php" ?>
</head>

<body>
	<?php
	$ACTIVE_TAB = 1;
	include "./common/header.php"
	?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2> Shell Accounts </h2>
				<p> bbssh.club provides shell accounts on a FreeBSD system. It's purpose is for everyone to learn and try out a new system. If you've never used FreeBSD or another UNIX this is a great way to try it! Shell accounts can also be used to connect to IRC networks, practice programming, networking, web development, and overall - make new friends. </p>
			</div>

			<div class="col-md-12">
				<h2> Modern, UNIX-friendly BBS-like Experience</h2>
				<p> In order to decrease the barrier to entry, we're actively developing menu-driven interface. New features are added semi-regularly. You don't need to know the ins and outs of UNIX to use bbssh!</p>
				<figure>
					<img src="./assets/images/demo.png" alt="Screenshot of bbssh.club's UI" />
					<figcaption> bbssh.club's BBS-like UI </figcaption>
				</figure>
			</div>

			<div class="col-md-12">
				<h2> Tiny Geocities-like Web Hosting </h2>
				<p> Each user is afforded 500MB of disk space. Not only is it intended to be used for web hosting, you can use it to play with source code and shell scripting too! A directory of web pages created by users can be found <a href="./users.php">here.</a></p>
			</div>

			<div class="col-md-12">
				<h2> Internal Mail System</h2>
				<p> To communite with other users, we use an internal mail system. This isn't e-mail, but it feels a lot like it. It's small, text based, and completly asyncronous. Currently most users access mail(1) while logged in but rsync and a cron job would be a cool idea for accessing your mail on a local machine.</p>
			</div>

			<div class="col-md-12">
				<h2> Possible upcoming features <small> ordered in estimated time of arrival</small></h2>
				<ul>
					<li>"cork board" system (simmilar to a real life cork board)</li>
					<li>menu-driven interface to mail(1)</li>
					<li>menu-driven "intro to the command line"</li>
					<li>functional registration system</li>
					<li>????</li>
					<li>profit</li>
				</ul>
			</div>



			<div class="col-md-6">
			</div>
		</div>
	</div>

	<?php include "./common/footer.php" ?>
</body>

</html>